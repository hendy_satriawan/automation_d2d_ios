*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

*** Variables ***
${timeout}    30s
*** Keywords ***
Permission Notifications
  ${per_notif}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Would Like to Send You Notifications"]    10s
  Run Keyword If    ${per_notif}    Click Element    //XCUIElementTypeButton[@name="Allow"]
  ...   ELSE   Log    Permission sudah di setujui

Permission Facebook
  ${per_notif}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Wants to Use “facebook.com” to Sign In"]
  Run Keyword If    ${per_notif}    Click Element    //XCUIElementTypeButton[@name="Continue"]
  ...   ELSE   Log    Permission sudah di setujui

Permission Google
  ${per_notif}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Wants to Use “google.com” to Sign In"]    10s
  Run Keyword If    ${per_notif}    Click Element    //XCUIElementTypeButton[@name="Continue"]
  ...   ELSE   Log    Permission sudah di setujui
#
Permission Calendar
  ${per_calendar}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Would Like to Access Your Calendar"]   20s
  Run Keyword If    ${per_calendar}    Click Element    //XCUIElementTypeButton[@name="OK"]
  ...   ELSE   Log    Permission sudah di setujui
#
Permission Photo
  ${per_photo}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Would Like to Access Your Photos"]    20s
  Run Keyword If    ${per_photo}    Click Element    //XCUIElementTypeButton[@name="OK"]
  ...   ELSE   Log    Permission sudah di setujui
#
# Coachmark Handle Got It
#   ${coachmark}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GOT IT"]
#   Run Keyword If    ${coachmark}    Click Element    //XCUIElementTypeButton[@name="GOT IT"]
#   ...   ELSE   Log    tidak ada Coachmark

Force Update
  # muncul halaman update
  ${cek_update}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Update D2D application ?"]   20s
  Run Keyword If    ${cek_update}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Later"]   20s
  ...   AND     Click Element   //XCUIElementTypeOther[@name="Later"]
  ...   ELSE    Log    Tidak Ada Update

Onboarding Handle
  Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Event"]   ${timeout}
  ${onboarding_event}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_continue"]
  Run Keyword If    ${onboarding_event}    Click Element    //XCUIElementTypeOther[@name="button_continue"]
  # cme
  Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="CME"]   ${timeout}
  ${onboarding_cme}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_continue"]
  Run Keyword If    ${onboarding_cme}    Click Element    //XCUIElementTypeOther[@name="button_continue"]
  # medicinus
  Wait Until Element Is Visible    //XCUIElementTypeOther[@name="Medicinus"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_start"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_start"]
  Permission Notifications
  # # masuk halaman login
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]   ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
