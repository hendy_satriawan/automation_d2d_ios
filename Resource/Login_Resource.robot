*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Profile_Resource.robot

*** Variables ***
#Data valid
${EMAIL_LOGIN_VALID}   Qa@yopmail.com
${PASSWORD_LOGIN_VALID}   123456
${nama_profile}   Fetuna CH         #prod
# ${nama_profile}   HAMDANI CHRISYANTO      #stag
# data login fb
${email_login_FB}   fetzune@gmail.com
${pass_login_FB}    Awan@123

${email_login_google}   hendy.satriawan@gmail.com

${EMAIL_LOGIN_EVENT}    ptgue.devbravo@gmail.com
${PASSWORD_LOGIN_EVENT}    guecuti
*** Keywords ***
Login Valid
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]    ${timeout}
  Tap    //XCUIElementTypeOther[@name="input_email"]
  Input Value    //XCUIElementTypeOther[@name="input_email"]    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  ${pwd}   Get Element Location    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]
  Log    ${pwd}
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]    ${timeout}
  Tap    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]
  Input Value    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="btnlogin"]
  # masuk ke halaman feeds
  Sleep    2s
  Permission Notifications
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonItemOnClick"]   ${timeout}

Login Invalid Email
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Email']
  Input Value    //XCUIElementTypeTextField[@value='Email']    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']    ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email tidak ditemukan, periksa kembali penulisan email Anda"]
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]


Login Invalid Password
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Email']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Email']
  Input Value    //XCUIElementTypeTextField[@value='Email']    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']    ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASSWORD_LOGIN_INVALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Login gagal. Silakan periksa password kamu!"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Valid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lupa sandi?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="Lupa sandi?"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]   ${timeout}
  # input email
  Tap    //XCUIElementTypeTextField[@value="Email"]
  Input Value    //XCUIElementTypeTextField[@value="Email"]    ${EMAIL_ALREADY}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]
  # ke halaman input kode verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sukses mengganti kata sandi!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # tampil halaman verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kode Verifikasi Password"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="VERIFIKASI AKUN"]    ${timeout}
  # klik verifikasi akun tanpa input code
  Click Element    //XCUIElementTypeButton[@name="VERIFIKASI AKUN"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="The Code field is required."]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Invalid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lupa sandi?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="Lupa sandi?"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]   ${timeout}
  # input email
  Tap    //XCUIElementTypeTextField[@value="Email"]
  Input Value    //XCUIElementTypeTextField[@value="Email"]    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="GANTI KATA SANDI"]
  # alert
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Permintaan tidak ditemukan, silakan periksa kembali."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Login Facebook
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnloginFB"]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  #pilih facebook
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnloginFB"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="btnloginFB"]
  Permission Facebook
  # masuk ke halaman facebook
  ${cek_fb_login}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Continue"]    10s
  Run Keyword If    ${cek_fb_login}    Click Element    //XCUIElementTypeButton[@name="Continue"]
  # belum pernah login fb
  Run Keyword Unless    ${cek_fb_login}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]    10s
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField
  ...   AND   Tap    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField
  ...   AND   Input Text    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField    ${email_login_FB}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeSecureTextField
  ...   AND   Tap    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeSecureTextField
  ...   AND   Input Text    //XCUIElementTypeOther[@name="main"]/XCUIElementTypeSecureTextField    ${pass_login_FB}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Masuk"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Continue"]
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Continue"]
  # cek halaman berhasil login
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}


Logout aplikasi
  #cek bila sudah otomatis login, dibuat logout agar kembali ke flow normal setelah fresh install
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“D2D” Would Like to Send You Notifications"]    10s
  Run Keyword If    ${cek_login}    Click Element    //XCUIElementTypeButton[@name="Allow"]
  ${cek_masuk_feed}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]
  Run Keyword If    ${cek_masuk_feed}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonProfile"]
  ...   AND   Click Element    //XCUIElementTypeOther[@name="buttonProfile"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_more"]   ${timeout}
  ...   AND   Click Element    xpath=(//XCUIElementTypeOther[@name="button_more"])[4]
  ...   AND   Wait Until Page Contains    Logout
  ...   AND   Click Text    Logout
  # ...   AND
  # ...   AND
  # ...   AND
  # ...   AND
  # ...   AND
  # ...   AND

Login Google
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnloginGoogle"]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  #pilih google
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnloginGoogle"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="btnloginGoogle"]
  Permission Google
  # masuk dengan akun google - belum pernah login
  ${cek_google_login}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Choose an account"]    ${timeout}
  Run Keyword If    ${cek_google_login}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${email_login_google}"]   ${timeout}
  ...   AND   Click Element    //XCUIElementTypeStaticText[@name="${email_login_google}"]
  # cek halaman berhasil login
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}

lupa password
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # pilih lupa password
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="forgot_password"]   ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="forgot_password"])[2]
  # masuk ke halaman lupa password
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot Password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]   ${timeout}
  # input email
  Tap    //XCUIElementTypeOther[@name="input_email"]
  Input Text    //XCUIElementTypeOther[@name="input_email"]    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_submit"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_submit"]
  # cek baerhasil input email lupa Password
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Check Your Email!"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We have sent a code to your email address"]    ${timeout}
  # back ke halaman sebelumnya
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_back"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Welcome!"]   ${timeout}

Login Akun Terdaftar Event
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]    ${timeout}
  Tap    //XCUIElementTypeOther[@name="input_email"]
  Input Value    //XCUIElementTypeOther[@name="input_email"]    ${EMAIL_LOGIN_EVENT}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  ${pwd}   Get Element Location    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]
  Log    ${pwd}
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]    ${timeout}
  Tap    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]
  Input Value    //XCUIElementTypeOther[contains(@name,'input_password ')][@height<="70"]    ${PASSWORD_LOGIN_EVENT}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="btnlogin"]
  # masuk ke halaman feeds
  Sleep    2s
  Permission Notifications
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
