*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***

Kembali Dari Detail Event Ke Feed
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}


Kembali Dari Detail Event Explore
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}

Kembali Dari Detail Journal Explore
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Learning"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_learning_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonSpecialist"]    ${timeout}

Kembali Dari Detail Journal ke Feeds
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}

Kembali Dari Detail Guideline ke Feeds
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}


Feeds Random
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_qr_scan"]    ${timeout}
  # buka konten yang muncul dengan random
  ${buttonon}   Run Keyword And Return Status    //XCUIElementTypeOther[@name="buttonItemOnClick"]
  Run Keyword If    ${buttonon}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonItemOnClick"]   ${timeout}
    ...   AND   Click Element    //XCUIElementTypeOther[@name="buttonItemOnClick"]
  Run Keyword If    '${buttonon}' == 'False'    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"]   ${timeout}
    ...   AND   Click Element    //XCUIElementTypeOther[@name="buttonOnClick"]
  # cek isi konten
  ${konten_event}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]   5s
  Log    ${konten_event}
  ${konten_journal}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Journal"]   5s
  Log    ${konten_journal}
  ${konten_guideline}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   5s
  Log    ${konten_guideline}
  ${konten_video}   Run Keywords    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]
  ...   AND   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Journal"]
  ...   AND   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]
  Log    ${konten_video}
  # eksplore konten
  Run Keyword If    ${konten_event}    Run Keywords    Konten Event
  ...   AND   Run Keyword    Kembali Dari Detail Event Ke Feed
  Run Keyword If    ${konten_journal}    Konten Journal
  Run Keyword If    ${konten_guideline}    Konten Guideline

Konten Event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]   ${timeout}
  # buka location / map
  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_open_location"]
  Sleep    2s
  ${cek_map}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Directions"]   ${timeout}
  # balik ke d2d kalau muncul maps
  Run Keyword If    ${cek_map}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="breadcrumb"]   ${timeout}
    ...   AND   Click Element    //XCUIElementTypeButton[@name="breadcrumb"]
    ...   AND   Sleep    2s
    ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]   ${timeout}
  # # download file announcement
  # ${cek_file}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_down_load_file"]
  # Run Keyword If    ${cek_file}    Run Keywords    Click Element    //XCUIElementTypeOther[@name="button_down_load_file"]
  # ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]   ${timeout}
  # ...   AND   Click Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]
  # ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]
  # scroll ke bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-feed}   Convert To String    ${lebars}
  ${x2-feed}   Convert To String    ${lebars}
  ${y1-feed}   Convert To String    ${tinggis}
  ${y2-feed}   Evaluate    ${tinggis} - 500
  #Scroll ke bawah
  : FOR    ${loopCount}    IN RANGE    0    3
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@text="Location"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # lakukan bookmark
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_bookmark"]    ${timeout}
  ${bookmark_label}    Get Element Location    //XCUIElementTypeOther[@name="button_bookmark"]
  Log    ${bookmark_label}
  Click Element    //XCUIElementTypeOther[@name="button_bookmark"]
  ## Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="bookmark succesfull"]    ${timeout}
  # add to calendar - later
  ${konfirm}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Information"]    10s
  Run Keyword If    ${konfirm}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Later"]    ${timeout}
    ...   AND   Click Element    //XCUIElementTypeButton[@name="Later"]
  # unbookmark
  # Set Global Variable    ${bookmark_label}
  ${bookmark_label}    Convert To String    ${bookmark_label}
  ${potong}   Remove String    ${bookmark_label}    {  '   y   x    :   }
  Log    ${potong}
  ${subsx}   Fetch From Right    ${potong}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${potong}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_bookmark"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element At Coordinates    ${subsx}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  ## Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="unbookmark succesfull"]    ${timeout}
  ${konfirm}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Information"]    10s
  Run Keyword If    ${konfirm}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Later"]    ${timeout}
    ...   AND   Click Element    //XCUIElementTypeButton[@name="Later"]
  # share
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_share"])[2]   ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_share"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # add to calendar - than cancel
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_calendar"]
  Sleep    5s
  Permission Calendar
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]   ${timeout}
  # unbookmark
  ## Set Global Variable    ${bookmark_label}
  ## ${bookmark_label}    Convert To String    ${bookmark_label}
  ## ${potong}   Remove String    ${bookmark_label}    {  '   y   x    :   }
  ## Log    ${potong}
  ## ${subsx}   Fetch From Right    ${potong}    ,
  ## ${subsx}   Fetch From Left    ${subsx}    .0
  ## ${subsy}   Fetch From Left    ${potong}    ,
  ## ${subsy}  Fetch From Left    ${subsy}    .0
  ## Log    ${subsx}
  ## Log    ${subsy}
  ## : FOR    ${loopCount}    IN RANGE    0    20
  ## \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_bookmark"]
  ## \    Run Keyword If    ${el}    Exit For Loop
  ## \    Click Element At Coordinates    ${subsx}    ${subsy}
  ## \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_bookmark"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_bookmark"]

Konten Journal
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Journal"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="see more..."]   ${timeout}
  # expand see more
  Click Element    //XCUIElementTypeOther[@name="see more..."]
  # scroll down & less
  # scroll ke bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-feed}   Convert To String    ${lebars}
  ${x2-feed}   Convert To String    ${lebars}
  ${y1-feed}   Convert To String    ${tinggis}
  ${y2-feed}   Evaluate    ${tinggis} - 500
  #Scroll ke bawah
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="less"]
  \    Run Keyword If    ${el}    Click Element    //XCUIElementTypeOther[@name="less"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # view pdf
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download_pdf"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_download_pdf"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]
  # download pdf
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_download"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]   ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download_pdf"]   ${timeout}
  Kembali Dari Detail Journal ke Feeds

Konten Guideline
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  Sleep    10s
  # download
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_download"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="QLOverlayDefaultActionButtonAccessibilityIdentifier"]    ${timeout}
  # keluar dari halaman preview
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]
  Kembali Dari Detail Guideline ke Feeds

# Feeds View Jurnal
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonFeeds')]
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonQRScan')]
#   #ambil judul
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]
#   ${judul_feed_jurnal}    Get Text    //android.widget.TextView[contains(@content-desc,'titleJournal')]
#   Log    ${judul_feed_jurnal}
#   #cari feed tipe journal
#   ${lebarx}    Get Window Width
#   ${tinggiy}   Get Window Height
#   ${lebarx}   Convert To Integer    ${lebarx}
#   ${tinggiy}  Convert To Integer    ${tinggiy}
#   ${lebars}   Evaluate    ${lebarx}/2
#   ${tinggis}    Evaluate    ${tinggiy} - 200
#   ${x1-feed}   Convert To String    ${lebars}
#   ${x2-feed}   Convert To String    ${lebars}
#   ${y1-feed}   Convert To String    ${tinggis}
#   ${y2-feed}   Evaluate    ${tinggis} - 500
#   #Scroll feed sampai dapat judul yang dimaksud (tipe journal)
#   : FOR    ${loopCount}    IN RANGE    0    20
#   \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.widget.TextView[contains(@text,'${judul_feed_jurnal}')]
#   \    Run Keyword If    ${el}    Exit For Loop
#   \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
#   \    ${loopCount}    Set Variable    ${loopCount}+1
#   Sleep    1s
#
# Feeds View Jurnal Detail
#   #ambil judul
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]
#   ${judul_feed_jurnal}    Get Text    //android.widget.TextView[contains(@content-desc,'titleJournal')]
#   Log    ${judul_feed_jurnal}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_jurnal}')]    ${timeout}
#   Click Element    //android.widget.TextView[contains(@text,'${judul_feed_jurnal}')]
#   #masuk ke detail feeds
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_jurnal}')]    ${timeout}
#   #klik see more & less
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonSeeMore')]   ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonSeeMore')]
#   Sleep    2s
#   #see less
#   ${lebarx}    Get Window Width
#   ${tinggiy}   Get Window Height
#   ${lebarx}   Convert To Integer    ${lebarx}
#   ${tinggiy}  Convert To Integer    ${tinggiy}
#   ${lebars}   Evaluate    ${lebarx}/2
#   ${tinggis}    Evaluate    ${tinggiy} - 200
#   ${x1-feed}   Convert To String    ${lebars}
#   ${x2-feed}   Convert To String    ${lebars}
#   ${y1-feed}   Convert To String    ${tinggis}
#   ${y2-feed}   Evaluate    ${tinggis} - 500
#   #Scroll feed sampai dapat judul yang dimaksud (tipe journal)
#   : FOR    ${loopCount}    IN RANGE    0    20
#   \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonSeeMore')]
#   \    Run Keyword If    ${el}    Exit For Loop
#   \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
#   \    ${loopCount}    Set Variable    ${loopCount}+1
#   Sleep    1s
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonDownloadPdf')]
#   Sleep    2s
#   #view pdf
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonDownlaod')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]   ${timeout}
#   Sleep    5s
#   #download pdf ke device
#   #Scroll feed sampai dapat judul yang dimaksud (tipe journal)
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonDownlaod')]
#   Permission_Storage
#   Sleep    1s
#
# Bookmark Feed Detail Jurnal
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonMore')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonMore')]
#   Wait Until Element Is Visible    //android.widget.TextView[contains(@text,'Bookmark')]    ${timeout}
#   Click Element    //android.widget.TextView[contains(@text,'Bookmark')]
#   #bookmark succesfull snackbar
#   # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
#
# Unbookmark Feed Detail Jurnal
#   Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonMore')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonMore')]
#   Wait Until Element Is Visible    //android.widget.TextView[contains(@text,'Unbookmark')]    ${timeout}
#   Click Element    //android.widget.TextView[contains(@text,'Unbookmark')]
#   #bookmark succesfull snackbar
#   # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'unbookmark succesfull')]    ${timeout}
#
# Bookmark Feed List Jurnal
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
#
# Unbookmark Feed List Jurnal
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'unbookmark succesfull')]    ${timeout}
#
# Kembali Dari PDF ke Detail Feed
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]   ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Journal')]    ${timeout}
#
# Kembali Dari Detail Jurnal ke List Feed
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonBack')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBack')]
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
#
# Feeds View Guideline
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonFeeds')]   ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonQRScan')]    ${timeout}
#   #cari feed tipe journal
#   ${lebarx}    Get Window Width
#   ${tinggiy}   Get Window Height
#   ${lebarx}   Convert To Integer    ${lebarx}
#   ${tinggiy}  Convert To Integer    ${tinggiy}
#   ${lebars}   Evaluate    ${lebarx}/2
#   ${tinggis}    Evaluate    ${tinggiy} - 200
#   ${x1-feed}   Convert To String    ${lebars}
#   ${x2-feed}   Convert To String    ${lebars}
#   ${y1-feed}   Convert To String    ${tinggis}
#   ${y2-feed}   Evaluate    ${tinggis} - 500
#   #Scroll feed sampai dapat judul yang dimaksud (tipe guideline)
#   : FOR    ${loopCount}    IN RANGE    0    20
#   \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]
#   \    Run Keyword If    ${el}    Exit For Loop
#   \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
#   \    ${loopCount}    Set Variable    ${loopCount}+1
#   Sleep    1s
#
# Feeds View Guideline Detail
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]    ${timeout}
#   Click Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]
#   #masuk ke pdf Guidleine
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonDownlaod')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]   ${timeout}
#   Sleep    5s
#   #download pdf ke device
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonDownlaod')]
#   Permission_Storage
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Downloading!')]    ${timeout}
#
# Kembali Dari Detail Guideline ke List Feed
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]    ${timeout}
#   Sleep    2s
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonClose')]
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]   ${timeout}
#
# Bookmark Feed List Guideline
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Guideline')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]   ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
#
# Unbookmark Feed List Guideline
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Guideline')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Guideline}')]   ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'unbookmark succesfull')]    ${timeout}
#
# Feeds View Video
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonFeeds')]   ${timeout}
#   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonQRScan')]    ${timeout}
#   #cari feed tipe journal
#   ${lebarx}    Get Window Width
#   ${tinggiy}   Get Window Height
#   ${lebarx}   Convert To Integer    ${lebarx}
#   ${tinggiy}  Convert To Integer    ${tinggiy}
#   ${lebars}   Evaluate    ${lebarx}/2
#   ${tinggis}    Evaluate    ${tinggiy} - 200
#   ${x1-feed}   Convert To String    ${lebars}
#   ${x2-feed}   Convert To String    ${lebars}
#   ${y1-feed}   Convert To String    ${tinggis}
#   ${y2-feed}   Evaluate    ${tinggis} - 500
#   #Scroll feed sampai dapat judul yang dimaksud (tipe guideline)
#   : FOR    ${loopCount}    IN RANGE    0    200
#   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Guideline')]
#   \    Run Keyword If    ${el}    Exit For Loop
#   \    Swipe    ${x1-feed}    ${y1-feed}    ${x2-feed}    ${y2-feed}
#   \    ${loopCount}    Set Variable    ${loopCount}+1
#   Sleep    1s
#   Click Element    //android.widget.TextView[contains(@text,'Guideline')]
#
# Feeds View Video Detail
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]    ${timeout}
#   Click Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]
#   #masuk ke video
#   Sleep    5s
#   #back
#   Press Keycode    4    #back
#   #back ke list feeds
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]   ${timeout}
#
# Bookmark Feed List Video
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Video')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]   ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
#
# Unbookmark Feed List Video
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Video')]    ${timeout}
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]   ${timeout}
#   Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
#   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]
#   #bookmark succesfull snackbar
#   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'unbookmark succesfull')]    ${timeout}
