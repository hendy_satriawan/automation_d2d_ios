*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Feeds_Resource.robot

*** Variables ***
${cari_webinar}    Workshop Update ANC & USG

*** Keywords ***
Explore Webinar
  # masuk ke list webinar
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonWebinar"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonWebinar"]
  # cek halaman webinar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Webinar"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_webinar_search"]     ${timeout}
  # scroll 3x kebawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll webinar
  : FOR    ${loopCount}    IN RANGE    0    2
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Cari Webinar
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_webinar_search"])[2]
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_webinar_search"])[2]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We are really sad we could not find anything"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search button_search"]      ${timeout}
  # cari webinar
  Click Element    //XCUIElementTypeOther[@name=" input_search button_search"]
  Input Text    //XCUIElementTypeOther[@name=" input_search button_search"]    ${cari_webinar}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # masuk ke detil webinar
  Sleep    2s
  # Click Element At Coordinates    20    80
  Click Element    xpath=(//XCUIElementTypeOther[@name="10 months ago  Workshop Update ANC & USG 1070 Views . 09 Mar 2019"])[5]
  # Get Element Location    xpath=(//XCUIElementTypeOther[contains(@name,'ago  titleWebinar')])[5]
  # Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[contains(@name,'ago  titleWebinar')])[5]    ${timeout}
  # Click Element    xpath=(//XCUIElementTypeOther[contains(@name,'ago  titleWebinar')])[5]
  # # keluar dari halaman cari
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  # Click Element    //XCUIElementTypeOther[@name="button_close"]
  # # cek halaman webinar
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Webinar"]    ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_webinar_search"]     ${timeout}

Explore Detil Webinar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Webinar Recorded"]     ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_webinar}"]    ${timeout}
  # like webinar
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_like"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_like"]
  Click Element    //XCUIElementTypeOther[@name="button_like"]
  # share
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_share"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # submit question - open chat
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_go_to_chat"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_go_to_chat"]
  # cek halaman chat
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Back to Webinar"]    ${timeout}
  ## Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Sign in to chat"]      ${timeout}
  ## Click Element    //XCUIElementTypeButton[@name="Sign in to chat"]
  ## Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="Email or phone"]    ${timeout}
  ## kembali ke detil wabinar
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_back"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Webinar Recorded"]     ${timeout}
  # kembali ke list webinar - dari cari
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  # keluar dari halamn cari ke list webinar
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]      ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_close"]
  # cek list webinar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Webinar"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_webinar_search"]     ${timeout}
