*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Variables ***
# === ini untuk ios ===
${REMOTE_URL}       http://0.0.0.0:4723/wd/hub
${app}    /Users/hendy.satriawan_gue/Documents/D2D_ios_app/D2D_18.app
${bundleId}   com.D2D.ios
${version}    12.1
${deviceName}    iphone X 12
${udid}    46F5A9E9-C232-45E0-84BA-2DFAD1CBF394
${platformName}    iOS
${automationName}    XCUITest
# "app": "/Users/hendy.satriawan_gue/Downloads/D2D.app"

*** Keywords ***
Buka apps D2D real device
  Open Application    ${REMOTE_URL}    platformName=${platformName}    platformVersion=${version}    deviceName=${deviceName}    app=${app}   udid=${udid}
  ...    automationName=${automationName}     noReset=False     useNewWDA=True    waitForQuiescence=false
  Permission Notifications
  Force Update
  ${cek_onboarding}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]
  Run Keyword If    ${cek_onboarding}    Onboarding Handle
  # Logout aplikasi
