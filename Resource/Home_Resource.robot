*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Question_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${nama_anak_newborn}    Ani Tumbuh Kembang

*** Keywords ***
Kembali Ke Homepage Fertil Dari Artikel Slider
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="CHECKLIST"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1

Kembali ke Home Newborn dari Add Record
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="baby_picture"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_album"]    ${timeout}

Kembali ke Home Newborn dari Add Album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="baby_picture"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_album"]    ${timeout}

Kembali ke Home Newborn dari Info Tumbuh Kembang
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="baby_picture"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_album"]    ${timeout}

Kembali ke Home Newborn dari Checklist
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="SEE MORE"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="TIPS"]   ${timeout}
  # check yang di homepage
  : FOR    ${loopCount}    IN RANGE    0    2
  \    Sleep    1s
  \    Click Element    //XCUIElementTypeButton[@name="check"]
  \    ${loopCount}    Set Variable    ${loopCount}+1

Homepage Program Hamil Artikel Slider 1
  ${judulslide1}     Get Text    //XCUIElementTypeStaticText[@name="5 Kesalahan Umum saat Menggunakan Test Pack"]
  Log    ${judulslide1}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide1}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="5 Kesalahan Umum saat Menggunakan Test Pack"]
  # masuk ke artikel slider
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide1}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  # scroll sampai halaman bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel dibawah)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Kesehatan"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Artikel Slider 2
  ${judulslide2}     Get Text    //XCUIElementTypeStaticText[@name="Yang Perlu Mums Ketahui Tentang Secondary Infertility"]
  Log    ${judulslide2}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide2}"]    ${timeout}
  Click Element    ${judulslide2}
  # masuk ke artikel slider
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulslide2}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  # scroll sampai halaman bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel dibawah)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Checklist
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  #scroll cari checklist
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat checklist)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # check awal
  # get element checklist untuk di unchecklist
  ${check1}   Get Element Location    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Log    ${check1}
  ${check1}    Convert To String    ${check1}
  ${remove}   Remove String    ${check1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  Click Element    //XCUIElementTypeStaticText[@name="Analisis kromosom untuk mengetahui risiko kelainan kromosom pada janin. "]
  Sleep    2s
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click A Point   x=${subsx}   y=${subsy}
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  # masuk kedalam checklist
  #scroll cari see more
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="SEE MORE"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Click Element    //XCUIElementTypeStaticText[@name="SEE MORE"]
  Program Hamil Checklist

Program Hamil Checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]
  # get element checklist untuk di unchecklist
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="check_box"]    ${timeout}
  ${check1}   Get Element Location    //XCUIElementTypeButton[@name="check_box"]
  Log    ${check1}
  ${check1}    Convert To String    ${check1}
  ${remove}   Remove String    ${check1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  # collapse pemeriksaan laboratorium
  Click Element    //XCUIElementTypeButton[@name="check_box"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  Sleep    1s
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click A Point   x=${subsx}   y=${subsy}
  # hide pemeriksaan laboratorium
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]

Kembali Ke Homepage Fertil Dari Checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="CHECKLIST"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # cek halaman homepage
  Sleep    5s

Homepage Program Hamil Tips
  #scroll cari tips
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll tips sampai bawah (sampai dapat tips)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke dalam tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]   ${timeout}
  ${tips1}  Get Text    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  Click Element    //XCUIElementTypeStaticText[@name="Bolehkah Bercinta saat Haid?"]
  Wait Until Page Contains    ${tips1}    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke artikel terkait
  ${tips2}  Get Text    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeTable/XCUIElementTypeCell[2]
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeTable/XCUIElementTypeCell[2]
  # cek halaman artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="TIPS"]
  #share tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]
  Click Element    //XCUIElementTypeButton[@name="share"]
  # cancel share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali Ke Homepage Fertil Dari Tips
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TIPS"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1

Homepage Program Hamil Tips Slide
  ${tipsslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Cukupi Kebutuhan Zat Besi"]
  ${tipsslide}    Convert To String    ${tipsslide}
  ${remove}   Remove String    ${tipsslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Kesalahan Umum Penggunaan Testpack"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Kesalahan Umum Penggunaan Testpack"]
  #Scroll tips sampai bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Homepage Program Hamil Artikel
  #scroll cari artikel
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll sampai bawah (sampai dapat artikel)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[14]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke artikel
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[14]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="GueSehat"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share grey"]   ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  #  buka artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   60s
  # cek artikel terkait
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="GueSehat"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share grey"]   ${timeout}

Kembali Ke Homepage Fertil Dari Artikel
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  #scroll sampai dapat artikel terkait
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="ARTIKEL TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Page Should Contain Element    //XCUIElementTypeButton[@name="ARTIKEL TERKAIT"]

Homepage Program Hamil Checklist Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="notif_button"]
  #masuk halaman notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]
  Click Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]
  # masuk ke halaman checklist
  Program Hamil Checklist

Kembali Ke Homepage Fertil Dari Checklist Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek halaman notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Notifikasi"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek halaman homepage
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="sudah_hamil"]    ${timeout}
  Sleep    5s

Kembali Ke homapage Fertil via Menu
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  # check halaman homepage
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton    ${timeout}

Ubah status Menjadi Sudah Hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton
  # verifikasi melanjutkan sudah Hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat atas Kehamilan Mums, silahkan klik Iya untuk melanjutkan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Iya"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Iya"]
  # masuk question sedang hamil -  pilih belum agar bisa input hpht & masuk maternity dari minggu awal
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apakah Mums sudah dinyatakan hamil oleh dokter?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BELUM"]  ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BELUM"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apa hasil testpack Mums ?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GARIS 1"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="GARIS 1"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi HPHT*"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="PILIH"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="PILIH"]
  # pilih tema yang menarik
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih tema yang paling menarik untuk Mums!"]   ${timeout}
  : FOR    ${loopCount}    IN RANGE    11    15
  \    ${el}    Log    ${loopCount}
  \    Sleep    1s
  \    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[${loopCount}]/XCUIElementTypeButton
  \    Run Keyword If    '${el}' == '14'    Exit For Loop
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    30s
  Click Element    //XCUIElementTypeButton[@name="SUBMIT"]
  # cek halaman maternity
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat atas kehamilan Mums!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TUTUP"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="TUTUP"]
  Coachmark Handle Got It

Sedang Hamil Homapage Janin 3D
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # ${janin}    Get Element Location    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]
  # Log    ${janin}


Homepage New Born Foto Baby
  #cek halaman new born
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="New Born"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="baby_picture"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="baby_picture"]
  # pilih kamera
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  # pilih foto
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # cek sukses ambil foto
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}

Cek upload Foto & Fitur This Week
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="2 Mgg"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="2 Mgg"]
  Sleep    3s
  # cek button this week, kalau tidak ada lakukan klik lagi pada tab 2mggu
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="this_week"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Run Keyword Unless    ${el}    Click Element    //XCUIElementTypeStaticText[@name="2 Mgg"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="this_week"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="this_week"]
  Sleep    3s

Record baby homepage newborn
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_record"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add_record"]
  # masuk ke halaman Record
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Berat badan si Kecil?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="4,4"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="NEXT"]   ${timeout}

Add Album Hompeage NewBorn
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_album"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add_album"]
  # masuk ke halaman add album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan foto-foto Mums"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Buat Album Baru"]    ${timeout}

Homepage New Born Info Tumbuh Kembang
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_album"]    ${timeout}
  #scroll sampai dapat bagian tumbuh kembang anak
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 400
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll sampai bawah (sampai dapat tumbuh kembang)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Mums, ayo perhatikan langkah yang harus dilakukan seminggu setelah persalinan!"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Mums, ayo perhatikan langkah yang harus dilakukan seminggu setelah persalinan!"]
  # masuk ke halaman tumbuh kembang
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tumbuh Kembang"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat! Bayi yang Mums Nanti Telah Lahir"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="NEW BORN"]   ${timeout}
  #Scroll sampai bawah (sampai tidak muncul judul tumbuh kembang)
  : FOR    ${loopCount}    IN RANGE    0    4
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton
  # cek halaman tumbuh kembang minggu ke 1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="3"]
  Sleep    1s
  # kembali ke info tumbuh kembang new born
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tumbuh Kembang"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat! Bayi yang Mums Nanti Telah Lahir"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="NEW BORN"]   ${timeout}

Homepage New Born Checklist
  #scroll cari checklist
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat checklist)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="SEE MORE"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke see more
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="SEE MORE"]
  Click Element    //XCUIElementTypeStaticText[@name="SEE MORE"]
  # cek halaman checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Perawatan di Rumah"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="check_off"]

Homepage New Born Notifikasi Checklist
  # masuk ke halaman notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="notif_button"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Notifikasi"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Press for more"]   ${timeout}
  ## masuk ke checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]
  ## cek halaman checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  ## New Born Checklist

New Born Checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Perawatan di Rumah"]   ${timeout}
  # get element checklist untuk di unchecklist
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="check_box"]    ${timeout}
  ${check1}   Get Element Location    //XCUIElementTypeButton[@name="check_box"]
  Log    ${check1}
  ${check1}    Convert To String    ${check1}
  ${remove}   Remove String    ${check1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  # checklist check box
  Click Element    //XCUIElementTypeButton[@name="check_box"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  Sleep    1s
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click A Point   x=${subsx}   y=${subsy}
  # hide tab perawatan dirumah
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Perawatan di Rumah"]

Kembali ke Homepage New Born dari checklist Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek halaman Notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Notifikasi"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo lengkapi checklist mums."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek homepage nwe born
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}

New Born Grafik
  # berat badan sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Berat badan si Kecil?"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # tinggi badan sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tinggi badan si Kecil?"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # lingkar kepala si kecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Lingkar kepala si Kecil?"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # grafik hasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Hasil"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SIMPAN GRAFIK"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SIMPAN GRAFIK"]
  # proses berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # show grafik berat
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]  ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kesimpulan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Berat"][@value="1"]    ${timeout}
  # show grafik tinggi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Tinggi"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"][@value="1"]   ${timeout}
  # show grafik kepala
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}
  # cek grafik dropdown
  #pilih grafik anak - berat badan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Berat Badan"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Berat Badan"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]  ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kesimpulan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Berat"][@value="1"]    ${timeout}
  #pilih grafik anak - tinggi badan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Tinggi Badan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Tinggi Badan"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Tinggi"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"][@value="1"]   ${timeout}
  #pilih grafik anak - lebar kepala
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Lingkar Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Lingkar Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}
  # pilih batal
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Batal"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Batal"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}

Homepage New Born Tips
  #scroll cari tips
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat tips)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="ARTIKEL TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # ke detail tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="NEWBORN"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="NEWBORN"]
  # cek detail tips
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="tips-back"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  # # scroll tips sampai tips terkait
  # : FOR    ${loopCount}    IN RANGE    0    5
  # \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="New Born"]
  # \    Sleep    1s
  # \    Run Keyword If    ${el}    Exit For Loop
  # \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  # \    ${loopCount}    Set Variable    ${loopCount}+1
  # Sleep    1s
  # ${tipsterkait}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="New Born"]
  # Run Keyword If    ${tipsterkait}    Click Element    //XCUIElementTypeStaticText[@name="New Born"]
  # Wait Until Page Contains Element    //XCUIElementTypeImage[@name="tips-back"]   ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  # share tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke Homepage New Born dari Detail Tips
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="side_menu"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="TIPS"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}


Homepage New Born Artikel
  #scroll cari artikel
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel)
  : FOR    ${loopCount}    IN RANGE    0    5
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[contains(@name,'Kesehatan')]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Kesehatan')]
  # ${tagkesehatan}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Kesehatan')]
  # ${tagtumbuh}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Tumbuh Kembang')]
  # ${tagmenyusui}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Menyusui')]
  # Run Keyword If    ${tagkesehatan}    Click Element    //XCUIElementTypeStaticText[contains(@name,'Kesehatan')]
  # Run Keyword If    ${tagtumbuh}    Click Element    //XCUIElementTypeStaticText[contains(@name,'Tumbuh Kembang')]
  # Run Keyword If    ${tagmenyusui}    Click Element    //XCUIElementTypeStaticText[contains(@name,'Menyusui')]
  # =======
  #cek halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="like"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  # # scroll sampai bawah
  # : FOR    ${loopCount}    IN RANGE    0    20
  # \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  # \    Sleep    1s
  # \    Run Keyword If    ${el}    Exit For Loop
  # \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  # \    ${loopCount}    Set Variable    ${loopCount}+1
  # lakukan share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share_button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke Homepage New Born dari Detail Artikel
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="notif_button"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Kesehatan')]    ${timeout}
