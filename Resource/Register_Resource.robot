*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
#Data valid
${NAMA_DEPAN}   yunia4
${NAMA_BELAKANG}    sintian1
${EMAIL_VALID}    yunsina2@yopmail.com
${EMAIL_VALID_2}    yunsin2@yopmail.com
${PASS_VALID}   123456
#Data Invalid
${EMAIL_INVALID_1}    yun@yopmail
${EMAIL_INVALID_2}    yunyopmail.com
${EMAIL_ALREADY}      aniula@yopmail.com
${PASS_INVALID}   12345

*** Keywords ***
Masuk Register Via Login
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  # cek syarat & ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Syarat dan Ketentuan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Syarat dan Ketentuan"]
  # masuk ke halaman
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Syarat & Ketentuan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dismiss"]   ${timeout}
  # kembali ke halaman login
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}

Register Email Sudah Terdaftar
  Buka apps temanbumil real device
  #masuk ke halaman register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Alamat Email']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Alamat Email']
  Input Value    //XCUIElementTypeTextField[@value='Alamat Email']    ${EMAIL_ALREADY}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert email sudah terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email anda sudah terdaftar, silakan login"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Email Tidak Valid 1
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Alamat Email']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Alamat Email']
  Input Value    //XCUIElementTypeTextField[@value='Alamat Email']    ${EMAIL_INVALID_1}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert email tidak valid
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Email Tidak Valid 2
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Alamat Email']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Alamat Email']
  Input Value    //XCUIElementTypeTextField[@value='Alamat Email']    ${EMAIL_INVALID_1}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert email tidak valid
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Password Tidak Valid
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Alamat Email']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Alamat Email']
  Input Value    //XCUIElementTypeTextField[@value='Alamat Email']    ${EMAIL_VALID}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_INVALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert password minimal 6 huruf
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Password minimal 6 huruf"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Tidak Pilih Setuju
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Alamat Email']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Alamat Email']
  Input Value    //XCUIElementTypeTextField[@value='Alamat Email']    ${EMAIL_VALID_2}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert password minimal 6 huruf
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Setuju syarat dan ketentuan"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Tidak Input Mandatory Field
  Buka apps temanbumil real device
  Permission Notifications
  #masuk ke halaman register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Register"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Register"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Depan']   ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Depan']
  Input Value    //XCUIElementTypeTextField[@value='Nama Depan']    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value='Nama Belakang']    ${timeout}
  Tap    //XCUIElementTypeTextField[@value='Nama Belakang']
  Input Value    //XCUIElementTypeTextField[@value='Nama Belakang']    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value='Password']   ${timeout}
  Tap    //XCUIElementTypeSecureTextField[@value='Password']
  Input Value    //XCUIElementTypeSecureTextField[@value='Password']    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Bergabung di Teman Bumil, Mums secara eksklusif akan mendapatkan informasi terbaru dari kami."]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="REGISTER"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="REGISTER"]
  # alert email sudah terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Dengan Akun Facebook
