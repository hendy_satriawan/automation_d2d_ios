*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Feeds_Resource.robot

*** Variables ***
${cari_event}   Konker PDPI 2019
${cari_event_berbayar}    KONAS XVIII PERHATI-KL 2019
${cari_spesialis}     Spesialis Bedah Saraf

${cari_video_spesialis}
${cari_guideline_spesialis}     Evidence Based Pediatrics
# ${cari_jurnal_spesialis}    Allergic Rhinitis and its Impact on Asthma (ARIA)       #stag
${cari_jurnal_spesialis}      Nasal High-flow Therapy in Infants and Children     #prod

${request_city}     Kota Jakarta Barat
${request_judul}      [ TEST DUMMY ]

${pilihobat}   White petrolatum generic
${cariobat}   Albumin

*** Keywords ***
Explore Tab Event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonExplore"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonExplore"]
  # pilih event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonEvent"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonEvent"]
  # masuk halaman event
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_filter"]     ${timeout}
  # buka event
  ${click}   Get Element Location    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]
  Log    ${click}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]
  Konten Event
  Kembali Dari Detail Event Ke Event

Kembali Dari Detail Event Ke Event
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_filter"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_filter"]     ${timeout}


Explore Tab Bookamrk Event
  # masuk detil lalu bookmark lagi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]
  # bookmark lagi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_bookmark"]    ${timeout}
  ${bookmark_label}    Get Element Location    //XCUIElementTypeOther[@name="button_bookmark"]
  Log    ${bookmark_label}
  Set Global Variable    ${bookmark_label}
  Log    ${bookmark_label}
  Click Element    //XCUIElementTypeOther[@name="button_bookmark"]
  # Click Element    //XCUIElementTypeOther[@name="button_bookmark"]
  # Calendar - later
  ${konfirm}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Information"]    10s
  Run Keyword If    ${konfirm}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Later"]    ${timeout}
    ...   AND   Click Element    //XCUIElementTypeButton[@name="Later"]
  # kembali ke list event
  Kembali Dari Detail Event Explore
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="BOOKMARK"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="BOOKMARK"]
  # cek yang sudah terbookmark
  : FOR    ${loopCount}    IN RANGE    0    20
  \    Sleep    3s
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="buttonOnClick"][@x=10]
  \    Run Keyword Unless    ${el}    Exit For Loop
  \    Run Keyword If    ${el}    unbookmark_event
  \    Sleep    3s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data not found"]   ${timeout}

unbookmark_event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"][@x=10]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonOnClick"][@x=10]
  # Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[45]    ${timeout}
  # Click Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[45]
  # lakukan unbookmark
  # ${bookmark_label}    Get Element Location    //XCUIElementTypeOther[@name="button_bookmark"]
  Log    ${bookmark_label}
  # unbookmark
  # ${bookmark_label}    Convert To String    ${bookmark_label}
  # ${potong}   Remove String    ${bookmark_label}    {  '   y   x    :   }
  # Log    ${potong}
  # ${subsx}   Fetch From Right    ${potong}    ,
  # ${subsx}   Fetch From Left    ${subsx}    .0
  # ${subsy}   Fetch From Left    ${potong}    ,
  # ${subsy}  Fetch From Left    ${subsy}    .0
  # Log    ${subsx}
  # Log    ${subsy}
  # : FOR    ${loopCount}    IN RANGE    0    20
  # \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_bookmark"]
  # \    Run Keyword If    ${el}    Exit For Loop
  # \    Click Element At Coordinates    ${subsx}    ${subsy}
  # \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_bookmark"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_bookmark"]
  ${konfirm}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Information"]    10s
  Run Keyword If    ${konfirm}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Later"]    ${timeout}
    ...   AND   Click Element    //XCUIElementTypeButton[@name="Later"]
  # kembali ke list bookmark
  Kembali Dari Detail Event Explore


Explore Tab Join Event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="JOINED"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="JOINED"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data not found"]   ${timeout}
  # kembali ke tab event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="EVENT"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="EVENT"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]     ${timeout}

Search Event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_search"]
  # masuk ke halaman search
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_search')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We are really sad we could not find anything"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  # lakukan search
  Click Element    //XCUIElementTypeOther[@name=" input_search "]
  Input Text    //XCUIElementTypeOther[@name=" input_search "]    ${cari_event}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # masuk ke yang dicari
  ${cekcari}    Run Keyword And Return Status    Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]
  Run Keyword If    ${cekcari}    Run Keywords    Click Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]
  ...   AND     Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]   ${timeout}
  ...   AND     Wait Until Page Contains Element    //XCUIElementTypeOther[@name="${cari_event}"]    ${timeout}



Kembali dari pencarian event ke list event
  ${backcari}     Run Keyword And Return Status    Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[4]
  Run Keyword If    ${backcari}    Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[4]
  # keluar dari search
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_search')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_close"]
  # cek halaman list event
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_filter"]   ${timeout}

Explore Learning Spesialis
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonExplore"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonExplore"]
  # pilih learning
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonLearning"]      ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonLearning"]
  # masuk ke halaman learning
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Learning"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_learning_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonSpecialist"]    ${timeout}
  # masuk ke spesialis
  Click Element    //XCUIElementTypeOther[@name="buttonSpecialist"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_more"]     ${timeout}
  # masuk ke konten
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonItemOnClick"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonItemOnClick"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_back"]

Explore Tab Bookmark Learning
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="tab_heading"])[2]      ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="tab_heading"])[2]


Explore Search Learning Spesialis
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_search"]
  # masuk halaman cari
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search "]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name=" input_search "]
  # cari jurnal
  Input Text    //XCUIElementTypeOther[@name=" input_search "]    ${cari_jurnal_spesialis}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # cek yang dicari - jurnal
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="JOURNAL"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="JOURNAL"]
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonItemOnClick"])[2]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Didn't found any journal?"]     ${timeout}
  # masuk ke konten yang dicari
  Click Element    xpath=(//XCUIElementTypeOther[@name="buttonItemOnClick"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name=" ${cari_jurnal_spesialis} "]    ${timeout}
  # kembali ke hasil pencarian
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Didn't found any journal?"]     ${timeout}
  # masuk ke reqest journal via search learning
  Click Element    //XCUIElementTypeOther[@name="Didn't found any journal?"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Request Journal"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]   ${timeout}
  # kembali ke hasil pencarian
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Didn't found any journal?"]     ${timeout}
  # keluar dari halaman pencarian & masuk lagi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_close"]
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  # Click Element    //XCUIElementTypeOther[@name="button_search"]
  # # masuk halaman cari
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search "]    ${timeout}
  # Click Element    //XCUIElementTypeOther[@name=" input_search "]
  # # cari guideline
  # Input Text    //XCUIElementTypeOther[@name=" input_search "]    ${cari_guideline_spesialis}
  # Click Element    //XCUIElementTypeButton[@name="Return"]
  # # cek yang dicari - guideline
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="GUIDELINE"]   ${timeout}
  # Click Element    //XCUIElementTypeOther[@name="GUIDELINE"]
  # Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonItemOnClick"])[2]      ${timeout}
  # # masuk ke konten yang dicari
  # Click Element    xpath=(//XCUIElementTypeOther[@name="buttonItemOnClick"])[7]
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_guideline_spesialis}"]    ${timeout}
  # # kembali ke hasil pencarian
  # Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]    ${timeout}
  # Click Element    xpath=(//XCUIElementTypeOther[@name="button_close"])[2]
  # # kembali ke halaman list spesialis
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  # Click Element    //XCUIElementTypeOther[@name="button_close"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  # cek halaman awal learning
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Learning"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_learning_search"]    ${timeout}

Search Spesialis
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_learning_search"])[2]   ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_learning_search"])[2]
  # masuk ke halaman search
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search button_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We are really sad we could not find anything"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  # lakukan search
  Click Element    //XCUIElementTypeOther[@name=" input_search button_search"]
  Input Text    //XCUIElementTypeOther[@name=" input_search button_search"]    ${cari_spesialis}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # cek yang dicari
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonSpecialist"])[4]     ${timeout}    # android 5
  Click Element    xpath=(//XCUIElementTypeOther[@name="buttonSpecialist"])[4]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_spesialis}"]      ${timeout}
  # kembali ke halaman awal learning - dari search
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]      ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search button_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]      ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_close"]
  # cek halaman awal learning
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Learning"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_learning_search"]    ${timeout}

Explore Request Journal
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonExplore"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonExplore"]
  # pilih request journal
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonRequestJournal"]      ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonRequestJournal"]
  # masuk halaman request JOURNAL
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Request Journal"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]   ${timeout}
  # input data request journal
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_city"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="input_city"]
  # cari kota
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search_city button_clear"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name=" input_search_city button_clear"]
  Input Text    //XCUIElementTypeOther[@name=" input_search_city button_clear"]    ${request_city}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # cek yang dicari
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="KOTA JAKARTA BARAT"])[4]     ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="KOTA JAKARTA BARAT"])[4]
  # input journal yang dicari
  # scroll artikel sampai bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel terkait)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_submit"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="title_journal Write the title and description of journal here"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="title_journal Write the title and description of journal here"]
  Input Text    //XCUIElementTypeOther[@name="title_journal Write the title and description of journal here"]    ${request_judul}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Need another journal?"]     ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Need another journal?"]
  Sleep    1s
  # klik submit
  #Scroll artikel sampai bawah (sampai dapat tombol submit)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_submit"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_submit"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_submit"]

Explore Event Dengan Materi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonExplore"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonExplore"]
  # pilih event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonEvent"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonEvent"]
  # masuk halaman event
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_filter"]     ${timeout}
  # masuk ke tab join
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="JOINED"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="JOINED"]
  # buka event di tab join untuk buka materi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonOnClick"][@visible="true"]
  ## # cari event
  ## Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]   ${timeout}
  ## Click Element    //XCUIElementTypeOther[@name="button_search"]
  ## # masuk ke halaman search
  ## Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_search')]    ${timeout}
  ## Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We are really sad we could not find anything"]     ${timeout}
  ## Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  ## # lakukan search
  ## Click Element    //XCUIElementTypeOther[@name=" input_search "]
  ## Input Text    //XCUIElementTypeOther[@name=" input_search "]    ${cari_event_berbayar}
  ## Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]     ${timeout}
  ## Click Element    //XCUIElementTypeButton[@name="Return"]
  ## # masuk ke event yang dicari
  ## Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]    ${timeout}
  ## Click Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]
  # cek halaman event dengan materi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_calendar"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_open_location"]     ${timeout}
  Wait Until Page Does Not Contain Element    //XCUIElementTypeOther[@name=" Download Materi Event Material File"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_materi"]    ${timeout}
  # buka materi
  Click Element    //XCUIElementTypeOther[@name="button_materi"]
  # cek halaman materi
  Sleep    2s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event Material"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" Aspek Medikolegal "]    ${timeout}
  # balik ke detil event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_back"]

Explore Obat A to Z
  # pilih explore & pilih menu obat a to z
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonExplore"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonExplore"]
  # pilih A to Z
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonAtoZ"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonAtoZ"]
  # masuk halaman A to Z
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Obat A to Z"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_input_cari_obat"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Acarbose generic"]     ${timeout}
  # swipe alphabet
  #ambil kordinat element
  ${slidealpabet}    Get Element Location    //XCUIElementTypeOther[@name="button_alphabet"][6][@visible="true"]
  ${slidealpabet}    Convert To String    ${slidealpabet}
  ${remove}   Remove String    ${slidealpabet}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_alphabet"][26][@visible="true"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Buka Detil Obat A to Z
  # buka obat dengan huruf W
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_alphabet"][23][@visible="true"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_alphabet"][23][@visible="true"]
  # masuk ke detial obat
  # scroll kebawah cari obat
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat obat yang dicari
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@label="${pilihobat}"]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
   # klik obat untuk ke detail
   ${klikobat}    Get Element Location    //XCUIElementTypeOther[@label="${pilihobat}"]
   ${klikobat}    Convert To String    ${klikobat}
   ${remove}   Remove String    ${klikobat}    {  '   y   x    :   }
   Log    ${remove}
   ${subsx}   Fetch From Right    ${remove}    ,
   ${subsx}   Fetch From Left    ${subsx}    .0
   ${subsy}   Fetch From Left    ${remove}    ,
   ${subsy}  Fetch From Left    ${subsy}    .0
   ${subsx2}  Fetch From Right    ${remove}    ,
   ${subsx2}   Fetch From Left    ${subsx2}    .0
   Log    ${subsx}
   Log    ${subsy}
   # #ubah value untuk digunakan untuk swipe
   Click Element At Coordinates    ${subsx}    ${subsy}
   # Click Element    //XCUIElementTypeOther[@name=${pilihobat}"]
   # cek halaman detil obat
   # Wait Until Page Contains Element    //XCUIElementTypeOther[@label="${pilihobat}"]    ${timeout}
   Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Penggunaan Obat"]    ${timeout}
   # see more & less
   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="show_more_less"]    ${timeout}
   Click Element    //XCUIElementTypeOther[@name="show_more_less"]
   Sleep    1s
   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="show_more_less"]    ${timeout}
   Click Element    //XCUIElementTypeOther[@name="show_more_less"]
   #swipe sampai dapat text related
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Related"][@visible="true"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # buka related
  ${related}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Heparin"]
  Run Keyword If    ${related}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Heparin"]    ${timeout}
    ...   AND   Click Element    //XCUIElementTypeOther[@name="Heparin"]
    ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]   ${timeout}
    ...   AND   Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name=" Heparin "]    ${timeout}
    ...   AND   Sleep    2s
    ...   AND   Click Element    //XCUIElementTypeOther[@name="button_back"]
    ...   AND   Wait Until Page Contains Element    //XCUIElementTypeOther[@name="${pilihobat}"][@visible="true"]    ${timeout}
  # kembali ke halaman obat A to Z
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_back"]
  # cek halaman obat A to Z
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Obat A to Z"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_input_cari_obat"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'${pilihobat}')][@visible="true"]    ${timeout}

Cari Obat A to Z
  # scroll kebawah cari obat
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  # masuk ke halaman cari obat
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_input_cari_obat"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_input_cari_obat"]
  # input obat yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name=" input_search button_clear_input"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
  Tap    //XCUIElementTypeOther[@name=" input_search button_clear_input"]
  Input Text    //XCUIElementTypeOther[@name=" input_search button_clear_input"]    ${cariobat}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # pilih obat yang dicari
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="Albumin generic"])[4]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="Albumin generic"])[4]
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Albumin generic"][@y="69"]     ${timeout}
  # Click Element    //XCUIElementTypeOther[@name="Albumin generic"][@y="69"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'${cariobat}')]    ${timeout}
  Click Element    //XCUIElementTypeOther[contains(@name,'${cariobat}')]
  # cek halaman yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cariobat}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Penggunaan Obat"]    ${timeout}
  # see more & less
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="show_more_less"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="show_more_less"]
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="show_more_less"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="show_more_less"]
  #swipe sampai dapat text related
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Related"]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
   # kembali ke halaman A to Z
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${ul}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Obat A to Z"]
   \    Run Keyword If    ${ul}    Exit For Loop
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]
   \    Run Keyword If    ${el}    Click Element    //XCUIElementTypeOther[@name="button_back"]
   \    Run Keyword Unless    ${el}   Click Element    //XCUIElementTypeOther[@name="button_close"]
   \    Sleep    1s
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
