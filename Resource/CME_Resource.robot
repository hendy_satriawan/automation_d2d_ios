*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Variables ***
# ${judul_kuis}    Quiz Everlasting     #stag
# ${judul_kuis}     Nilai Diagnostik Pemeriksaan ICT Fetrin Pada Anemia Defisiensi Besi     #prod
${judul_kuis}     Akurasi tes monofilamen untuk neuropatika diabetika      #prod
${title_course}   SKP manual Test
${source}     Titan Center
${skp_poin}     2



*** Keywords ***
Kembali ke Halaman CME
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="CME"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_cme_history"]    ${timeout}

CME Kuis
  #masuk ke cme
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonCme"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonCme"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="CME"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_cme_history"]    ${timeout}
  # mulai kuis
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'${judul_kuis}')]    ${timeout}
  Click Element    //XCUIElementTypeOther[contains(@name,'${judul_kuis}')]
  # masuk ke halaman kuis
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_kuis}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_start_quiz"]    ${timeout}
  # isi soal
  Click Element    //XCUIElementTypeOther[@name="button_start_quiz"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Questions"]    ${timeout}
  # klik submit tanpa isi
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'radio 0')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_submit"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_submit"]
  # hasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="You may try again next time"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_ok"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_ok"]
  # kembali ke halaman cme
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="CME"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_cme_history"]    ${timeout}


CME SKP Manual
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonCmeKursus"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonCmeKursus"]
  # input skp / course manual
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="CME Course"]   ${timeout}
  # input title
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_title"]   ${timeout}
  Tap    //XCUIElementTypeOther[@name="input_title"]
  Input Text    //XCUIElementTypeOther[@name="input_title"]    ${title_course}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # input source
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_course"]      ${timeout}
  Tap    //XCUIElementTypeOther[@name="input_course"]
  Input Text    //XCUIElementTypeOther[@name="input_course"]    ${source}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # input skp poin
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_skp_point"]     ${timeout}
  Tap    //XCUIElementTypeOther[@name="input_skp_point"]
  Input Text    //XCUIElementTypeOther[@name="input_skp_point"]    ${skp_poin}
  Click Element    //XCUIElementTypeStaticText[@name="SKP Point"]
  # tanggal sertifikat
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_date "]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="input_date "]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Pick a date"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Confirm"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Confirm"]
  # upload sertifikat
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_show_choices"]   ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_show_choices"]
  #pilih dari galeri
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_gallery"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_gallery"]
  Permission Photo
  # pilih foto
  ${capability}   Get Capability    platformVersion
  Log    ${capability}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Run Keyword If    '${capability}' == '10.2'    Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Run Keyword If    '${capability}' == '12.1'   Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]     ${timeout}
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  ...    AND    Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="Upload"])[2]    ${timeout}
  Sleep    2s
  # upload
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_send_email"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    xpath=(//XCUIElementTypeOther[@name="Upload"])[2]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # cek hasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Certificate"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_send_email"]   ${timeout}
  # sent email
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_send_email"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_send_email"])[2]
  # download
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_download"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_download"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="QLOverlayDoneButtonAccessibilityIdentifier"]
  Kembali ke Halaman CME

CME History
  # masuk ke history
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_cme_history"])[2]     ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_cme_history"])[2]
  # halaman history
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="History"]    ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="D2D SKP"]   ${timeout}
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="MANUAL SKP"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="tab_heading"]     ${timeout}
  # pilih manual skp
  ${manualskp}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="MANUAL SKP"]
  Run Keyword If    ${manualskp}    Click Element    //XCUIElementTypeOther[@name="MANUAL SKP"]
  Run Keyword Unless    ${manualskp}    Click Element    xpath=(//XCUIElementTypeOther[@name="tab_heading"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'${title_course} ${skp_poin} SKP')]   ${timeout}
  # kembali ke halaman cme
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="CME"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_cme_history"]    ${timeout}
