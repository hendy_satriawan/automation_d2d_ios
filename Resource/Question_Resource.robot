*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Profile_Resource.robot
*** Variables ***
# [_thisWeekButton setAccessibilityLabel:@"this_week"];
# [_pictureButton setAccessibilityLabel:@"baby_picture"];
${NAMA_BAYI}    MyBaby
${BERAT_BAYI}   3
${PANJANG_BAYI}   50
${LINGKAR_BAYI}   33
${NAMA_BAYI_EDIT}   MyBaby-Edit
${nama_anak_newborn}    Ani Tumbuh Kembang
*** Keywords ***
Question Program Hamil
  Cek & Hapus Data Yang Sudah Ada
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="background pop up"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan tambah data anak Mums terlebih dahulu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # masuk ke question page
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]
  # halaman apa yang harus saya pilih
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih kategori ini jika Mums berencana untuk hamil lagi atau sedang menjalani program kehamilan."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]
  # pilih program hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="PROGRAM HAMIL"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="PROGRAM HAMIL"]
  # yakin.?
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Apakah Mums yakin ingin menggunakan fitur Program Hamil?"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Iya"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Iya"]
  # cek create sukses
  Coachmark Handle Got It
  # # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Program Hamil"]    ${timeout}
  # # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profile"]    ${timeout}
  # ke Home klik back - jika sebelumnya buat anak dari profile
  ${profile}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Program Hamil"]    ${timeout}
  Run Keyword If    ${profile}    Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]   ${timeout}
  Run Keyword If    ${profile}    Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[1]
  #cek halaman home
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}

Cek & Hapus Data Yang Sudah Ada
  ${dataanak}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeImage[@name="background pop up"]
  Run Keyword If    ${dataanak}    Coachmark Handle Got It
  Run Keyword Unless    ${dataanak}    Hapus Data Semua Program


Question Newborn
  Cek & Hapus Data Yang Sudah Ada
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="background pop up"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan tambah data anak Mums terlebih dahulu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # masuk ke question page
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Apa yang harus saya pilih?"]
  # halaman apa yang harus saya pilih
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih kategori ini jika Mums berencana untuk hamil lagi atau sedang menjalani program kehamilan."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK, SAYA MENGERTI"]
  # pilih program New Born
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TUMBUH KEMBANG"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="TUMBUH KEMBANG"]
  # isi tanggal lahir sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi tanggal lahir si Kecil"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LANJUT"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LANJUT"]
  # isi nama & jenis kelamin sikecil
  # isi nama anak
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi nama si Kecil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Nama Anak"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@value="Nama Anak"]    ${nama_anak_newborn}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih jenis kelamin
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Jenis Kelamin"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@value="Jenis Kelamin"]
  # hitungan date time picker untuk swipe
  ${picker}   Get Element Location    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther
  Log    ${picker}
  ${tinggi}   Get Window Height
  ${lebar}    Get Window Width
  ${picker}    Convert To String    ${picker}
  ${remove}   Remove String    ${picker}    {  '   y   x    :   }
  Log    ${remove}
  Log    ${tinggi}
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsy}
  ${subsy}    Evaluate    ${subsy} / 2
  Log    ${subsy}
  ${pickerhight}    Evaluate    ${tinggi} - ${subsy}
  Log    ${pickerhight}
  ${lebars}    Evaluate    ${lebar} / 2
  Log    ${lebars}
  ${pickerstart}    Evaluate    ${pickerhight} + 45
  Swipe    ${lebars}    ${pickerstart}    ${lebars}    ${pickerhight}
  Wait Until Page Contains Element    //XCUIElementTypePickerWheel[@value="Laki-laki"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeTextField[2][@value="Laki-laki"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LANJUT"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LANJUT"]
  # isi data anak
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi data anak saat lahir"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[3]
  Input Text    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[3]    5
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[1]
  Input Text    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[1]    6
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[2]
  Input Text    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeTextField[2]    7
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LANJUT"]
  Click Element    //XCUIElementTypeButton[@name="LANJUT"]
  # pilih tema
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pilih tema yang paling menarik untuk Mums!"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Kesehatan"]
  Click Element    //XCUIElementTypeStaticText[@name="Menyusui"]
  Click Element    //XCUIElementTypeStaticText[@name="Psikologi"]
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[14]/XCUIElementTypeButton   #//XCUIElementTypeStaticText[@name="Tumbuh Kembang"]
  Click Element    //XCUIElementTypeStaticText[@name="MPASI"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SUBMIT"]
  Click Element    //XCUIElementTypeButton[@name="SUBMIT"]
  # Coachmark Handle Got It
  Kembali Dari Profile Ke Homepage Newborn
