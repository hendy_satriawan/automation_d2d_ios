*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Home_Resource.robot
Resource    ../Resource/Question_Resource.robot

*** Variables ***
${judulagenda}    Konsultasi ke Bidan
${isiagenda}   Konsultasi Program Hamil Ke Bidan
${judulagendaedit}    Konsultasi ke Bidan - edit
${namaalbum}    Album Program Hamil
${namaalbumedit}    Album Program Hamil - edit
${judulartikel}   Ini Pertanda Masa Subur
${cariartikel}    Mengenal Infeksi Rahim Penyebab Infertilitas
${judultips}    Alat Deteksi Masa Subur
${caritips}    Cukupi Kebutuhan Zat Besi
${judul_agenda_nb}   Konsultasi Tumbuh Kembang Anak
${judul_agenda_nb_edit}   Konsultasi Tumbuh Kembang Anak - Edit
${isi_agenda_nb}    Konsultasi perihal perkembangan anak
${edit_berat}   5
${edit_tinggi}    6
${edit_kepala}    7
${judul_album_nb}   Album Bayi New Born
${judul_album_nb_edit}    Album Bayi New Born Edit
${cari_artikel_nb}    Jangan Lakukan Kebiasan Ini Jika Tak Ingin Produksi ASI Berkurang!
${judul_artikel_nb}   Ini Alasan Kenapa Kontak Kulit Penting untuk Bayi Baru Lahir
${judul_tips_nb}    Tips Memandikan Bayi Baru Lahir
${cari_tips_nb}   Tips Memilih Pompa ASI
${cari_thread_nb}   tes darah kehamilan
${judul_resep_6bln}   Sari Jeruk Anggur
${cari_resep_nb}    Sosis Gulung Mi

*** Keywords ***
Program Hamil Menu Checklis
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  # check halaman checklist
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pemeriksaan Laboratorium"]    ${timeout}
  # checklist
  Program Hamil Checklist Fertil

Program Hamil Menu Agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  # buka menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Agenda"]
  #masuk ke agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="add"]   ${timeout}
  # add agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add"]
  # masuk ke halaman tambah agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Agenda"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan"]   ${timeout}
  #input agenda
  # input judul
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Judul"]  ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Judul"]    ${judulagenda}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Agenda"]
  # input isi agenda
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Isi Agenda"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Isi Agenda"]    ${isiagenda}
  Click Element    //XCUIElementTypeButton[@name="Next:"]
  # pilih tanggal
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="tanggal_agenda"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="tanggal_agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih waktu kegiatan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="waktu_agenda"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="waktu_agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih pengingat
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="reminder_agenda"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="reminder_agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik simpan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="simpan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # berhasil simpan
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  # klik oke berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek agenda yang sudah dibuat
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add"]  ${timeout}
  Page Should Contain Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  # share agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  # buka Agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  # masuk ke dalam agenda - ubah jadi complete / uncomplete
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="share"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]   ${timeout}
  # complete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="COMPLETE"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Page Should Not Contain Element    //XCUIElementTypeStaticText[@name="${judulagenda}"]
  Page Should Contain Element    //XCUIElementTypeStaticText[@name="${isiagenda}"]
  # uncomplete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # edit agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="option"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ubah"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ubah"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ubah Agenda"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan"]   ${timeout}
  #hapus judul
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="hapus_judul"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="hapus_judul"]
  #edit judul
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="txt_judulagenda"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="txt_judulagenda"]    ${judulagendaedit}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # berhasil edit judul
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # hapus agenda via home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  # klik agenda di home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="AGENDA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="AGENDA"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulagendaedit}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulagendaedit}"]
  #share agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # hapus agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="option"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # sukses hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Program Hamil Menu ALbum
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  #buka menu pilih album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Album"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan foto-foto Mums"]    ${timeout}
  # create album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Buat Album Baru"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Buat Album Baru"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Buat Album"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Simpan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Name Album"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@value="Name Album"]    ${namaalbum}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  Click Element    //XCUIElementTypeButton[@name="Simpan"]
  # berhasil create album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek album yang sudah dibuat
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbum}"]   ${timeout}
  #tambah foto via kamera
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Moments"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Moments"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # tambah gambar berhasil
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   60s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeButton[1]    ${timeout}
  #masuk ke detail album
  ${album1}   Get Element Location    //XCUIElementTypeStaticText[@name="${namaalbum}"]
  Log    ${album1}
  ${album1}    Convert To String    ${album1}
  ${remove}   Remove String    ${album1}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  Click A Point   x=${subsx}   y=${subsy}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Daftar Album"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Daftar Album"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbum}"]   ${timeout}
  # tambah gambar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Moments"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Moments"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # tambah gambar berhasil
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   60s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeImage[1]    ${timeout}
  # edit album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Update"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Update"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Update Album"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Update Album"]
  # edit nama
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="delete_nama"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="delete_nama"]
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="nama_album"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="nama_album"]    ${namaalbumedit}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # berhasil edit album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbumedit}"]   ${timeout}
  # share gambar
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # closes gambar
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${namaalbumedit}"]   ${timeout}
  # hapus album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # verifikasi hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Yakin album di hapus?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ya"]
  Click Element    //XCUIElementTypeButton[@name="Ya"]
  # berhasil edit album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # kembali ke halaman album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}

Program Hamil Menu Artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Artikel"]
  # masuk ke halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # open artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judulartikel}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="like"]   ${timeout}
  # klik like
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="You have liked this!"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="like"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # share artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share_button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # scroll artikel sampai bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel terkait)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Kembali ke List Artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Search Artikel Program Hamil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk ke halaman search
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="tab_artikel"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="field_search"]    ${cariartikel}
  # Click Element    //XCUIElementTypeButton[@name="Search"] - saat di klik masih force close

Program Hamil Menu Tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  #pilih menu tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tips"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # open tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judultips}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judultips}"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judultips}"]    ${timeout}
  # scroll sampai dapat tips terkait
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-tips}   Convert To String    ${lebars}
  ${x2-tips}   Convert To String    ${lebars}
  ${y1-tips}   Convert To String    ${tinggis}
  ${y2-tips}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel terkait)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-tips}    ${y1-tips}    ${x2-tips}    ${y2-tips}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke list tips
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Search Tips Program Hamil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk halaman cari
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="tab_tips"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="field_search"]    ${caritips}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeOther[1]   ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeOther[1]
  # masuk tips yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${caritips}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dismiss"]   ${timeout}
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke List Tips Dari Cari
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Kembali Ke Homepage Fertil Dari Drawer Menu
  Wait Until Page Contains Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]    ${timeout}
  Click Element    //XCUIElementTypeApplication[@name="Teman Bumil"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[2]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika Mums sudah hamil klik di sini"]   ${timeout}

Drawer Menu Chekclist New Born
  # buka side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  # masuk menu checklist
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Checklist"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Checklist"]    ${timeout}
  New Born Checklist

Drawer Menu Agenda New Born
  # buka side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  # masuk menu agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add"]    ${timeout}
  # pilih tanggal untuk buat agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="7"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="7"]
  # add agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Agenda"]    ${timeout}
  # input judul agenda
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="judul_agenda"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="judul_agenda"]    ${judul_agenda_nb}
  Click Element    //XCUIElementTypeButton[@name="Next:"]
  # input isi agenda
  Input Text    //XCUIElementTypeTextField[@name="isi_agenda"]    ${isi_agenda_nb}
  Click Element    //XCUIElementTypeButton[@name="Next:"]
  # pilih tanggal
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih waktu kegiatan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="waktu_agenda"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="waktu_agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # pilih waktu pengingat
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="reminder_agenda"]   ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="reminder_agenda"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik simpan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="simpan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # sukses create agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # share agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # masuk ke agenda
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_agenda_nb}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judul_agenda_nb}"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]   ${timeout}
  # complete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="COMPLETE"]
  # share agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # UNCOMPLETE
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]
  # ubah agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="option"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ubah"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ubah"]
  # ubah judul
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="txt_judulagenda"]   ${timeout}
  Clear Text    //XCUIElementTypeTextField[@name="txt_judulagenda"]
  Input Text    //XCUIElementTypeTextField[@name="txt_judulagenda"]    ${judul_agenda_nb_edit}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="simpan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # berhasil Ubah
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # uncomplete & complete
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="COMPLETE"]
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="UNCOMPLETE"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="COMPLETE"]   ${timeout}
  # option batal
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="option"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Batal"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Batal"]
  # hapus agenda
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="option"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # hapus berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Agenda"]   ${timeout}


Drawer Menu Grafik Anak New Born
  #masuk ke menu grafik
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]
  # cek halaman grafik
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data Terakhir Janin"]    ${timeout}
  # input data grafik anak
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_grafik_baby"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add_grafik_baby"]
  # input berat sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Berat badan si Kecil?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="4,4"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="NEXT"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # input tinggi sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tinggi badan si Kecil?"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="53,7"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="NEXT"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # input lingkar kepala sikecil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Lingkar kepala si Kecil?"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="36,9"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="NEXT"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="NEXT"]
  # hasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Hasil"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="date_save"]   ${timeout}
  # pilih tanggal simpan
  Click Element    //XCUIElementTypeTextField[@name="date_save"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Done"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SIMPAN GRAFIK"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SIMPAN GRAFIK"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek grafik
  # show grafik berat
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]  ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kesimpulan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Berat"][@value="1"]    ${timeout}
  # show grafik tinggi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Tinggi"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"][@value="1"]   ${timeout}
  # show grafik kepala
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}
  # cek grafik dropdown
  #pilih grafik anak - berat badan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Berat Badan"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Berat Badan"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]  ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kesimpulan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Berat"][@value="1"]    ${timeout}
  #pilih grafik anak - tinggi badan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Tinggi Badan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Tinggi Badan"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Tinggi"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tinggi"][@value="1"]   ${timeout}
  #pilih grafik anak - lebar kepala
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Grafik Anak - Lingkar Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Grafik Anak - Lingkar Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kepala"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}
  # pilih batal
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrowdown white"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="arrowdown white"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Batal"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Batal"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kepala"][@value="1"]   ${timeout}
  # add data from grafik result
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="add_grafik_baby"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="add_grafik_baby"]
  New Born Grafik

Kembali ke Home Grafik New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  #cek halaman grafik
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data Terakhir Janin"]    ${timeout}

Edit Data Janin New Born
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data Terakhir Janin"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="edit_data_janin"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="edit_data_janin"]
  # masuk ke edit
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Isi data si Kecil saat lahir"]   ${timeout}
  # edit berat
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="berat_edit"]    ${timeout}
  Clear Text    //XCUIElementTypeTextField[@name="berat_edit"]
  Input Text    //XCUIElementTypeTextField[@name="berat_edit"]    ${edit_berat}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # edit tinggi
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="tinggi_edit"]   ${timeout}
  Clear Text    //XCUIElementTypeTextField[@name="tinggi_edit"]
  Input Text    //XCUIElementTypeTextField[@name="tinggi_edit"]    ${edit_tinggi}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # edit kepala
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="head_edit"]   ${timeout}
  Clear Text    //XCUIElementTypeTextField[@name="head_edit"]
  Input Text    //XCUIElementTypeTextField[@name="head_edit"]    ${edit_kepala}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  #submit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="submit_edit"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="submit_edit"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # cek halaman grafik
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Grafik Anak"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Data Terakhir Janin"]    ${timeout}

Drawer Menu Album New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Album"]
  # masuk halaman album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Album"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Simpan foto-foto Mums"]    ${timeout}
  # buat album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Buat Album Baru"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Buat Album Baru"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Buat Album"]   ${timeout}
  # input nama album
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="txt_album"]   ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="txt_album"]    ${judul_album_nb}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Simpan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Simpan"]
  # create album berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_album_nb}"]    ${timeout}
  # input gambar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  #pilih foto
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # masuk ke album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_album_nb}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="title_album"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="title_album"]
  # cek detail album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Daftar Album"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_album_nb}"]    ${timeout}
  # tambah foto
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tambah Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kamera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kamera"]
  #pilih foto
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # edit album
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Update"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Update"]
  # edit judul album
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Update Album"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="nama_album"]    ${timeout}
  Clear Text    //XCUIElementTypeTextField[@name="nama_album"]
  Input Text    //XCUIElementTypeTextField[@name="nama_album"]    ${judul_album_nb_edit}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="simpan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="simpan"]
  # berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_album_nb_edit}"]   ${timeout}
  # hapus
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="options"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="options"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hapus"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hapus"]
  # konfirmasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Yakin album di hapus?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ya"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ya"]
  # berhasil hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Page Should Not Contain Text    ${judul_album_nb_edit}

Drawer Menu Artikel New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Artikel"]
  # masuk menu artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # masuk ke kategori filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="filter"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="filter"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kategori Filter"]    ${timeout}
  # kembali ke list artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek list artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # masuk artikel
  #scroll cari artikel
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  # # cari artikel di tab new born
  # : FOR    ${loopCount}    IN RANGE    0    10
  # \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Lakukan 5 Cara Ini Jika si Kecil Menggigit Saat Menyusu!"]
  # \    Sleep    1s
  # \    Run Keyword If    ${el}    Exit For Loop
  # \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  # \    ${loopCount}    Set Variable    ${loopCount}+1
  # Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_artikel_nb}"]    ${timeout}
  ${artikelget}     Get Element Location    //XCUIElementTypeStaticText[@name="${judul_artikel_nb}"]
  ${artikelget}   Convert To String    ${artikelget}
  ${artikelget}   Remove String    ${artikelget}    {  '   y   x    :   }
  ${nilai1}    Fetch From Left    ${artikelget}    ,
  ${nilai2}    Fetch From Right    ${artikelget}    ,
  ${nilai1}   Fetch From Left    ${nilai1}    .
  ${nilai2}   Fetch From Left    ${nilai2}    .
  Log    ${nilai1}
  Log    ${nilai2}
  Click Element At Coordinates    ${nilai2}    ${nilai1}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="like"]   ${timeout}
  # scroll sampai bawah
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # share artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share_button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Cari artikel New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk halaman pencarian artikel
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="New Born"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="New Born"]
  # lakukan pencarian
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="field_search"]    ${cari_artikel_nb}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  # buka artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_artikel_nb}"]   ${timeout}
  ${artikelcari}     Get Element Location    //XCUIElementTypeStaticText[@name="${cari_artikel_nb}"]
  ${artikelcari}   Convert To String    ${artikelcari}
  ${artikelcari}   Remove String    ${artikelcari}    {  '   y   x    :   }
  ${nilai3}    Fetch From Left    ${artikelcari}    ,
  ${nilai4}    Fetch From Right    ${artikelcari}    ,
  ${nilai3}   Fetch From Left    ${nilai3}    .
  ${nilai4}   Fetch From Left    ${nilai4}    .
  Log    ${nilai3}
  Log    ${nilai4}
  Click Element At Coordinates    ${nilai4}    ${nilai3}
  # Click Element    //XCUIElementTypeStaticText[@name="${cari_artikel_nb}"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jangan Lakukan Kebiasan Ini Jika Tak Ingin Produksi ASI Berkurang!"]   ${timeout}
  #scroll cari artikel
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  # scroll sampai bawah
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Artikel Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="share_button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali ke List Artikel dari Cari
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Drawer Menu Tips New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tips"]
  # masuk menu tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  # cari tips - Tips memandikan bayi baru lahir
  #scroll cari tips
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  # cari tips di tab new born
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="${judul_tips_nb}"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # masuk ke tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_tips_nb}"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${judul_tips_nb}"]
  # cek halaman tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_tips_nb}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="share"]    ${timeout}
  # scroll sampai tips terkait
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="TIPS TERKAIT"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Kembali ke List Tips New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # cek halaman list tips
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}

Cari Tips New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk ke halaman cari tips
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon-search-grey"]    ${timeout}
  # input tips yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_search"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="field_search"]
  Input Text    //XCUIElementTypeTextField[@name="field_search"]    ${cari_tips_nb}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  # buka tips yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips Memilih Pompa ASI"]   ${timeout}

Kembali ke List Tips New Born dari Cari
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  # cek halaman list tips artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tips"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}

Drawer Menu Forum
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forum"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Forum"]
  # cek halaman forum
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Mulai Diskusi"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forum"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search_button"]    ${timeout}

Create Thread Forum New Born
  # create thread
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon pencil White"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon pencil White"]
  # masuk ke halaman create thread lalu kembali ke forum utama
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Choose Category"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  # create forum betulan
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Mulai Diskusi"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Mulai Diskusi"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Choose Category"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Fertilitas"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Fertilitas"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Miom"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Miom"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="New Thread"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Attach Image"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="submit_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="submit_button"]
  # validasi harus input
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Judul Tidak Boleh Kosong"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Kembali ke list Forum
  # klik iacon back dengan point x,y
  ${point}   Get Element Location    //XCUIElementTypeButton[@name="back_arrow"]
  Log    ${point}
  ${point}    Convert To String    ${point}
  ${remove}   Remove String    ${point}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  Log    ${subsx}
  Log    ${subsy}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="back_arrow"]   ${timeout}
  # Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="bookmark"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click A Point   x=${subsy}   y=${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Open Detail Thread Forum
  #buka thread yang dibuat oleh akun admin teman bumil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]
  # masuk ke detail thread
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="reply_button"]   ${timeout}
  # reply thread
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="reply_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="reply_button"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Reply"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="attach_image"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="attach_image"]
  #pilih Attach - cancel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SUBMIT"]
  # keluar halaman reply
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]  ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  # bookmark
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark_label"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="bookmark_label"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}

Cari Thread
  #scroll cari thread
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  # masuk ke halaman thread
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search_button"]
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="search_field"]    ${timeout}
  # input thread yang dicari
  Click Element    //XCUIElementTypeTextField[@name="search_field"]
  Input Text    //XCUIElementTypeTextField[@name="search_field"]    ${cari_thread_nb}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  Sleep    3s
  # hasil
  # cari threadnya
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="${cari_thread_nb}"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_thread_nb}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${cari_thread_nb}"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  # masuk ke halaman detail thread
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_thread_nb}"]    ${timeout}
  # reply thread
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="reply_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="reply_button"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Reply"]    ${timeout}
  # attach image
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="attach_image"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="attach_image"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Camera"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Camera"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Moments"]    ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Moments"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]     ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  # klik submit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SUBMIT"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SUBMIT"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Komentar Tidak Boleh Kosong"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # keluar dari reply
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]  ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  # reply quote
  # cari reply quote
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="reply_quote"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeButton[@name="reply_quote"]
  # masuk ke halaman reply quote
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Reply"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Delete Quote"]     ${timeout}
  # keluar dari reply
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]  ${timeout}
  Click Element    //XCUIElementTypeButton[@name="close"]
  # bookmark
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark_label"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="bookmark_label"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}

Buka Bookmark Thread
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="bookmark"]
  # masuk ke halaman bookmark
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bookmarks"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark_label"]   ${timeout}
  # unbookmark treads
  # cari reply quote
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Bookmark Mums masih kosong, ayo temukan forum menarik lainya"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="bookmark_label"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bookmark Mums masih kosong, ayo temukan forum menarik lainya"]   ${timeout}

Drawer Menu Resep New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Resep"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Resep"]
  # masuk ke halaman resep
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Resep"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon bookmark grey"]   ${timeout}

Buka Resep
  #scroll cari thread
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  # filter resep
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="filter"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="filter"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Filter Resep"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="check off"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="check off"]
  Kembali ke list resep
  # buka resep
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="6-8 Bulan"]    ${timeout}
  # scroll untuk cari resep
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="${judul_resep_6bln}"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="${judul_resep_6bln}"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${judul_resep_6bln}"]    ${timeout}
  # scroll sampai bawah
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="bookmark_label"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # bookmark
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="bookmark"]
  # Wait Until Page Does Not Contain Element    //XCUIElementTypeStaticText[@name="Resep Berhasil di Bookmark"]   ${timeout}

Kembali ke list resep
  ${backbutton}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="arrow_back"]
  Run Keyword If    ${backbutton}    Click Element    //XCUIElementTypeButton[@name="arrow_back"]
  ...   ELSE IF   '${backbutton}' == 'False'  Run Keyword    Click Element    //XCUIElementTypeButton[@name="back_arrow"]
  ${back2}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]
  Run Keyword If    ${back2}    Click Element    //XCUIElementTypeButton[@name="close"]
  # cek halaman list resep
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Resep"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon bookmark grey"]   ${timeout}

Cari Resep
  #scroll cari thread
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 600
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="search"]
  # masuk ke halaman cari
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="field_cari"]    ${timeout}
  Click Element    //XCUIElementTypeTextField[@name="field_cari"]
  Input Text    //XCUIElementTypeTextField[@name="field_cari"]    ${cari_resep_nb}
  Click Element    //XCUIElementTypeButton[@name="Search"]
  # buka hasil cari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sosis Gulung Mi"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Sosis Gulung Mi"]
  # masuk ke detail resep
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sosis Gulung Mi"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark"]     ${timeout}
  # scroll tips sampai bawah
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Resep Terkait"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # bookmark
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="bookmark"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="bookmark"]
  # Wait Until Page Does Not Contain Element    //XCUIElementTypeStaticText[@name="Resep Berhasil di Bookmark"]   ${timeout}

Buka Bookmark Resep
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Bookmark"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Bookmark"]
  # masuk halaman bookmark
  Wait Until Page Does Not Contain Element    //XCUIElementTypeActivityIndicator[@name="In progress"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon bookmark grey"]     ${timeout}
  # unbookmark semua
  : FOR    ${loopCount}    IN RANGE    0    10
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Bookmark Mums masih kosong, ayo temukan resep menarik lainya"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="icon bookmark grey"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bookmark Mums masih kosong, ayo temukan resep menarik lainya"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="All"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="All"]


Tambah Anak New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]
