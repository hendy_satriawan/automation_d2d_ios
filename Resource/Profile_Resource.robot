*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Feeds_Resource.robot

*** Variables ***

*** Keywords ***
Explore Profile
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonProfile"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonProfile"]
  # masuk halaman profile
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_profile}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="upload_profile"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   ${timeout}
  # upate profile - klik save
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 500
  #Scroll halaman sampai dapat tombol save
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="button_save"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeOther[@name="button_save"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="tab_PROFILE"]     ${timeout}
  # scroll ke atas sampai dapat upload image
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - ${tinggiy}
  ${x1-home}   Convert To String    ${lebars}
  ${x2-home}   Convert To String    ${lebars}
  ${y1-home}   Convert To String    ${tinggis}
  ${y1-home}   Evaluate    ${tinggis} + 300
  ${y2-home}   Evaluate    ${y1-home} + 300
  Log    ${x1-home}
  Log    ${x2-home}
  Log    ${y1-home}
  Log    ${y2-home}
  #Scroll artikel sampai atas - dapat upload image
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${eh}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeOther[@name="upload_profile"]
  \    Run Keyword If    ${eh}    Exit For Loop
  \    Swipe    ${x1-home}    ${y1-home}    ${x2-home}    ${y2-home}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Upload Foto Profile
  # upload foto
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="upload_profile"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="upload_profile"]
  ${capability}   Get Capability    platformVersion
  Log    ${capability}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Run Keyword If    '${capability}' == '10.2'    Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Run Keyword If    '${capability}' == '12.1'   Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Choose from Library…"]
  ...    AND    Click Element    //XCUIElementTypeButton[@name="Choose from Library…"]
  ...    AND    Permission Photo
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]    ${timeout}
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]     ${timeout}
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]

  # cek barhsiol Upload
  # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_profile}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="upload_profile"]    ${timeout}

Tab Bookmark & Download
  # tab bookmark
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_bookmark"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="button_bookmark"]
  # cek halaman bookmark
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bookmark"]     ${timeout}
  Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]    ${timeout}
  # kembali ke profile
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="${nama_profile}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   ${timeout}
  # masuk ke tab download
  Click Element    //XCUIElementTypeOther[@name="button_download"]
  # cek halaman download
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Download"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_back"]     ${timeout}
  # balik ke halaman profile
  Click Element    xpath=(//XCUIElementTypeOther[@name="button_back"])[2]
  # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="${nama_profile}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   ${timeout}


Logout D2D using hidden button
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="buttonProfile"]     ${timeout}
  Click Element    //XCUIElementTypeOther[@name="buttonProfile"]
  # masuk halaman profile
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="upload_profile"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_download"]   ${timeout}
  # klik logout
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 500
  #Scroll halaman sampai dapat tombol hiden logout
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Specialization"]
  \    Sleep    1s
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_logout_testing"]
  Click Element    //XCUIElementTypeOther[@name="button_logout_testing"]
  # cek halaman login
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="btnlogin"]    ${timeout}
