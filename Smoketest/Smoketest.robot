*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Feeds_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/CME_Resource.robot
Resource    ../Resource/Explore_Resource.robot
Resource    ../Resource/Webinar_Resource.robot
Resource    ../Resource/Profile_Resource.robot
# [Teardown]  Close Application

*** Test Cases ***
1.Login dengan Akun D2D Normal
  [Tags]    Login dengan menggunakan akun D2D yang sudah diregister
  Buka apps D2D real device
  Login Valid
  Logout D2D using hidden button
  Close Application

2.Login dengan menggunakan akun facebook
  [Tags]    Login dengan menggunakan akun facebook
  Buka apps D2D real device
  Login Facebook
  Logout D2D using hidden button
  Close Application

3.Login dengan menggunakan akun Google
  [Tags]    Login dengan menggunakan akun google
  Buka apps D2D real device
  Login Google
  Logout D2D using hidden button
  Close Application

4.Forgot Password
  [Tags]    lakukan forgot password dengan akun yang digunakan login normal
  Buka apps D2D real device
  lupa password
  Close Application

5.Feeds
#   [Tags]    masuk ke halaman feeds lalu buka konten yang ada
  Buka apps D2D real device
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Feeds"]
  Run Keyword Unless    ${cek_login}    Login Valid
  Feeds Random

6.CME
  [Tags]    masuk ke halaman CME lalu lakukan isi kuis & upload skp
  CME Kuis
  CME SKP Manual
  CME History

7.Explore - Event
  [Tags]    masuk ke halaman explore lalu eksplore event
  Explore Tab Event
  Explore Tab Bookamrk Event
  Explore Tab Join Event
  Search Event
  Kembali dari pencarian event ke list event

8.Explore - Learning
  [Tags]    masuk ke halaman explore lalu eksplore Learnin
  Explore Learning Spesialis
  Explore Tab Bookmark Learning
  Explore Search Learning Spesialis
  Search Spesialis

9.Explore - Request Journal
  [Tags]    masuk ke halaman explore lalu lakukan request journal
  Explore Request Journal

11.Explore - Obat A to Z
  [Documentation]   Buka Explore & Pilih Obat A to Z
  Explore Obat A to Z
  Buka Detil Obat A to Z
  Cari Obat A to Z

12.Webinar
  [Tags]    masuk ke halaman webinar lalu explore
  Explore Webinar
  Cari Webinar
  Explore Detil Webinar

13.Profile
  [Tags]    masuk ke halaman profile lalu explore
  Explore Profile
  Upload Foto Profile
  Tab Bookmark & Download
  Close Application

# ##Spesial Case
#
12.Event - Materi Download
  [Tags]    masuk dengan akun lain, lalu downlod materi event terdaftar
  Buka apps D2D real device
  Login Akun Terdaftar Event
  Explore Event Dengan Materi
  Close Application
